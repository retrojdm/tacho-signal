EESchema Schematic File Version 2
LIBS:power
LIBS:device
LIBS:transistors
LIBS:conn
LIBS:linear
LIBS:regul
LIBS:74xx
LIBS:cmos4000
LIBS:adc-dac
LIBS:memory
LIBS:xilinx
LIBS:microcontrollers
LIBS:dsp
LIBS:microchip
LIBS:analog_switches
LIBS:motorola
LIBS:texas
LIBS:intel
LIBS:audio
LIBS:interface
LIBS:digital-audio
LIBS:philips
LIBS:display
LIBS:cypress
LIBS:siliconi
LIBS:opto
LIBS:atmel
LIBS:contrib
LIBS:valves
EELAYER 25 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 1
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L D D1
U 1 1 59BF77F7
P 4550 3900
F 0 "D1" H 4550 3800 50  0000 C CNN
F 1 "1N4004" V 4550 3700 50  0000 R CNN
F 2 "Diodes_THT:D_DO-41_SOD81_P10.16mm_Horizontal" H 4550 3900 50  0001 C CNN
F 3 "" H 4550 3900 50  0001 C CNN
	1    4550 3900
	-1   0    0    1   
$EndComp
$Comp
L D_Zener D2
U 1 1 59BF7842
P 4950 3900
F 0 "D2" H 4950 4000 50  0000 C CNN
F 1 "Zener 110V .5W" V 4950 4100 50  0000 L CNN
F 2 "Diodes_THT:D_DO-41_SOD81_P10.16mm_Horizontal" H 4950 3900 50  0001 C CNN
F 3 "" H 4950 3900 50  0001 C CNN
	1    4950 3900
	1    0    0    -1  
$EndComp
$Comp
L R R1
U 1 1 59BF78A9
P 5350 3900
F 0 "R1" V 5430 3900 50  0000 C CNN
F 1 "1K" V 5350 3900 50  0000 C CNN
F 2 "Resistors_THT:R_Axial_DIN0207_L6.3mm_D2.5mm_P10.16mm_Horizontal" V 5280 3900 50  0001 C CNN
F 3 "" H 5350 3900 50  0001 C CNN
	1    5350 3900
	0    -1   -1   0   
$EndComp
$Comp
L R R2
U 1 1 59BF790E
P 5750 3900
F 0 "R2" V 5830 3900 50  0000 C CNN
F 1 "100" V 5750 3900 50  0000 C CNN
F 2 "Resistors_THT:R_Axial_DIN0207_L6.3mm_D2.5mm_P10.16mm_Horizontal" V 5680 3900 50  0001 C CNN
F 3 "" H 5750 3900 50  0001 C CNN
	1    5750 3900
	0    -1   -1   0   
$EndComp
$Comp
L 4N25 U1
U 1 1 59BF79A7
P 6300 4000
F 0 "U1" H 6100 4200 50  0000 L CNN
F 1 "H11L1" H 6300 4200 50  0000 L CNN
F 2 "Housings_DIP:DIP-6_W10.16mm" H 6100 3800 50  0001 L CIN
F 3 "" H 6300 4000 50  0001 L CNN
	1    6300 4000
	1    0    0    -1  
$EndComp
Wire Wire Line
	6000 3900 5900 3900
Wire Wire Line
	5600 3900 5500 3900
Wire Wire Line
	5200 3900 5100 3900
Wire Wire Line
	4800 3900 4700 3900
Wire Wire Line
	4400 3900 4300 3900
Wire Wire Line
	4300 4100 6000 4100
$Comp
L R R3
U 1 1 59BF7AF5
P 6850 3900
F 0 "R3" V 6930 3900 50  0000 C CNN
F 1 "10K" V 6850 3900 50  0000 C CNN
F 2 "Resistors_THT:R_Axial_DIN0207_L6.3mm_D2.5mm_P10.16mm_Horizontal" V 6780 3900 50  0001 C CNN
F 3 "" H 6850 3900 50  0001 C CNN
	1    6850 3900
	0    -1   -1   0   
$EndComp
$Comp
L CP C1
U 1 1 59BF7ECD
P 7100 3650
F 0 "C1" H 7125 3750 50  0000 L CNN
F 1 "0.1uF" H 7125 3550 50  0000 L CNN
F 2 "Capacitors_THT:CP_Radial_Tantal_D4.5mm_P5.00mm" H 7138 3500 50  0001 C CNN
F 3 "" H 7100 3650 50  0001 C CNN
	1    7100 3650
	-1   0    0    1   
$EndComp
Wire Wire Line
	6600 4000 7200 4000
Wire Wire Line
	7000 3900 7100 3900
Wire Wire Line
	7100 3900 7200 3900
Wire Wire Line
	6600 4100 7100 4100
Wire Wire Line
	7100 4100 7200 4100
Wire Wire Line
	7100 4100 7100 4200
Connection ~ 7100 4100
Connection ~ 7100 3900
Wire Wire Line
	6600 3900 6700 3900
Wire Wire Line
	7100 3800 7100 3900
$Comp
L GND #PWR01
U 1 1 59BF9369
P 7100 4200
F 0 "#PWR01" H 7100 3950 50  0001 C CNN
F 1 "GND" H 7100 4050 50  0000 C CNN
F 2 "" H 7100 4200 50  0001 C CNN
F 3 "" H 7100 4200 50  0001 C CNN
	1    7100 4200
	1    0    0    -1  
$EndComp
Wire Wire Line
	7100 3300 7100 3500
$Comp
L GND #PWR02
U 1 1 59BF93F1
P 7300 3300
F 0 "#PWR02" H 7300 3050 50  0001 C CNN
F 1 "GND" H 7300 3150 50  0000 C CNN
F 2 "" H 7300 3300 50  0001 C CNN
F 3 "" H 7300 3300 50  0001 C CNN
	1    7300 3300
	1    0    0    -1  
$EndComp
Wire Wire Line
	7100 3300 7300 3300
$Comp
L Screw_Terminal_01x03 J2
U 1 1 59BF7CC7
P 7400 4000
F 0 "J2" H 7400 4200 50  0000 C CNN
F 1 "Screw_Terminal_1x03" V 7500 4000 50  0000 C CNN
F 2 "Connectors_Terminal_Blocks:TerminalBlock_Pheonix_MKDS1.5-3pol" H 7400 4000 50  0001 C CNN
F 3 "" H 7400 4000 50  0001 C CNN
	1    7400 4000
	1    0    0    -1  
$EndComp
$Comp
L Screw_Terminal_01x02 J1
U 1 1 59BF76C1
P 4100 4100
F 0 "J1" H 4100 4200 50  0000 C CNN
F 1 "Screw_Terminal_1x02" V 4200 4100 50  0000 C CNN
F 2 "Connectors_Terminal_Blocks:TerminalBlock_Pheonix_MKDS1.5-2pol" H 4100 4100 50  0001 C CNN
F 3 "" H 4100 4100 50  0001 C CNN
	1    4100 4100
	-1   0    0    1   
$EndComp
Wire Wire Line
	4300 3900 4300 4000
$EndSCHEMATC
